﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
//using PG;
using System.Net;

public partial class test : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string msg = "";

        //PG.PaymentGateway objPG = new PaymentGateway();
        try
        {

            // msg = objPG.PgRefundAmount("f120c2adZvl4qBQb35435435", "Flight", "D", Convert.ToDouble(3), "aba", "434", "agentid", "PNRReject", 0, "",0,"");

            // Response.Write(msg);


            LblIPAddress.Text = GetIPAddress();
            REMOTE_ADDR.Text = GetIPAddressREMOTE_ADDR();
            HTTP_X_FORWARDED_FOR.Text = GetIPAddressFORWARDED_FOR();
            UserLocalHost.Text = GetIPAddressUserHostAddress();


            Response.Write(GetIPAddressTest());

           
        }
        catch(Exception ex)
        {
            Response.Write(ex.Message);
           // objPG.InsertExceptionLog("PaymentGateway", "PaymentGateway", "GetPgTransCharges", "SELECT", ex.Message, ex.StackTrace);
        }
    
    }
    protected string GetIPAddressTest()
    {
        string ipAddress;
        // Look for a proxy address first
        ipAddress = Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

        // If there is no proxy, get the standard remote address
        if (ipAddress == "" || ipAddress == null)
            ipAddress = Request.ServerVariables["REMOTE_ADDR"];

        return ipAddress;
    }


    protected string GetIPAddress()
    {
        System.Web.HttpContext context = System.Web.HttpContext.Current;
        string IpAddress = string.Empty;
        try
        {
            string ipAddress = context.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
            if (!string.IsNullOrEmpty(ipAddress))
            {
                string[] addresses = ipAddress.Split(',');
                if (addresses.Length != 0)
                {
                    IpAddress = addresses[0];
                }
            }
            else
            {
                IpAddress = context.Request.ServerVariables["REMOTE_ADDR"];
            }
        }
        catch (Exception ex)
        {

        }
        return IpAddress;

    }

    protected string GetIPAddressREMOTE_ADDR()
    {
        System.Web.HttpContext context = System.Web.HttpContext.Current;
        string IpAddress = string.Empty;
        try
        {
            IpAddress = context.Request.ServerVariables["REMOTE_ADDR"];
        }
        catch (Exception ex)
        {

        }
        return IpAddress;

    }

    protected string GetIPAddressFORWARDED_FOR()
    {
        System.Web.HttpContext context = System.Web.HttpContext.Current;
        string IpAddress = string.Empty;
        try
        {
            IpAddress = context.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
        }
        catch (Exception ex)
        {

        }
        return IpAddress;

    }

    protected string GetIPAddressUserHostAddress()
    {
        System.Web.HttpContext context = System.Web.HttpContext.Current;
        string IpAddress = string.Empty;
        //try
        //{
        //    IpAddress = context.Request.UserHostAddress;
        //}
        //catch (Exception ex)
        //{

        //}      

        //if (GetLan && string.IsNullOrEmpty(visitorIPAddress))
        //{
        //This is for Local(LAN) Connected ID Address
        string stringHostName = Dns.GetHostName();
        //Get Ip Host Entry
        IPHostEntry ipHostEntries = Dns.GetHostEntry(stringHostName);
        //Get Ip Address From The Ip Host Entry Address List
        IPAddress[] arrIpAddress = ipHostEntries.AddressList;

        try
        {
            IpAddress = arrIpAddress[arrIpAddress.Length - 2].ToString();
        }
        catch
        {
            try
            {
                IpAddress = arrIpAddress[0].ToString();
            }
            catch
            {
                try
                {
                    arrIpAddress = Dns.GetHostAddresses(stringHostName);
                    IpAddress = arrIpAddress[0].ToString();
                }
                catch
                {
                    IpAddress = "127.0.0.1";
                }
            }
        }
        // }
        return IpAddress;

    }

}