﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="HoldBusUpdate.aspx.cs" Inherits="BS_HoldBusUpdate" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title> 

     <link href="../../CSS/foundation.min.css" rel="stylesheet" />
    <link href="../../CSS/foundation.css" rel="stylesheet" />
    <link href="../../CSS/main2.css" rel="stylesheet" type="text/css" />
    <link href="../../CSS/style.css" rel="stylesheet" type="text/css" />
    <script src="//code.jquery.com/jquery-1.11.2.min.js"></script>
    <script type="text/javascript" >     
        $(document).ready(function () {       
            $('#Button1').click(function () {
                if (!$.trim($('#txt_PNR').val())) {
                    alert("PNR can't be empty!");
                    return false;
                }
                if (!$.trim($('#Txt_tktno').val())) {
                    alert("Ticket no can't be empty!");
                    return false;
                }
            });
        });
        </script>
</head>
<body>
    <form id="form1" runat="server">
          <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <div>
            &nbsp;
        </div>
        <div align="center">
            <table width="90%">
                <tr>
                    <td align="right" style="font-weight: bold; font-size: 15px; color: #004b91;">Booking Reference No.
                    </td>
                    <td id="tdRefNo" runat="server" align="left" style="font-weight: bold;"></td>
                </tr>
            </table>

        
            <div>
                &nbsp;
            </div>
              <table width="90%" class="tbltbl" border="0" cellpadding="10" cellspacing="10">
                <tr>
                    <td align="left" style="font-weight: bold; font-size: 14px;">Passenger Information
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                            <ContentTemplate>
                                <asp:GridView ID="GvTravellerInformation" runat="server" AutoGenerateColumns="false"
                                    Width="100%" CssClass="GridViewStyle">
                                    <Columns>

                                          <asp:TemplateField HeaderText="Order Id">
                                            <ItemTemplate>
                                                <asp:Label ID="lblTktNo" runat="server" Text='<%#Eval("ORDERID")%>'></asp:Label>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:TextBox ID="txtTktNo" runat="server" Width="80px" Text='<%#Eval("ORDERID")%>'></asp:TextBox>
                                            </EditItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField Visible="false">
                                            <ItemTemplate>
                                                <asp:Label ID="lblPaxId" runat="server" Text='<%#Eval("PAX_ID")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Title">
                                            <ItemTemplate>
                                                <asp:Label ID="lblTitle" runat="server" Text='<%#Eval("PAX_TITLE")%>'></asp:Label>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:TextBox ID="txtTitle" Width="50px" runat="server" Text='<%#Eval("PAX_TITLE")%>'></asp:TextBox>
                                            </EditItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Pax Name">
                                            <ItemTemplate>
                                                <asp:Label ID="lblFName" runat="server" Text='<%#Eval("PAXNAME")%>'></asp:Label>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:TextBox ID="txtFname" Width="100px" runat="server" Text='<%#Eval("PAXNAME")%>'></asp:TextBox>
                                            </EditItemTemplate>
                                        </asp:TemplateField>
                                          <asp:TemplateField HeaderText="Type">
                                            <ItemTemplate>
                                                <asp:Label ID="lblType" runat="server" Text='<%#Eval("GENDER")%>'></asp:Label>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:TextBox ID="txtType" MaxLength="3" runat="server" Width="60px" Text='<%#Eval("GENDER")%>'></asp:TextBox>
                                            </EditItemTemplate>
                                        </asp:TemplateField>

                                          <asp:TemplateField HeaderText="Seat NO">
                                            <ItemTemplate>
                                                <asp:Label ID="lblType" runat="server" Text='<%#Eval("SEATNO")%>'></asp:Label>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:TextBox ID="txtType" MaxLength="3" runat="server" Width="60px" Text='<%#Eval("SEATNO")%>'></asp:TextBox>
                                            </EditItemTemplate>
                                        </asp:TemplateField>


                                          <asp:TemplateField HeaderText="SOURCE">
                                            <ItemTemplate>
                                                <asp:Label ID="lblSOURCE" runat="server" Text='<%#Eval("SOURCE")%>'></asp:Label>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:TextBox ID="txtSOURCE" MaxLength="3" runat="server" Width="60px" Text='<%#Eval("SOURCE")%>'></asp:TextBox>
                                            </EditItemTemplate>
                                        </asp:TemplateField>

                                          <asp:TemplateField HeaderText="DESTINATION">
                                            <ItemTemplate>
                                                <asp:Label ID="lblDESTINATION" runat="server" Text='<%#Eval("DESTINATION")%>'></asp:Label>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:TextBox ID="txtDESTINATION" MaxLength="3" runat="server" Width="60px" Text='<%#Eval("DESTINATION")%>'></asp:TextBox>
                                            </EditItemTemplate>
                                        </asp:TemplateField>

                                         <asp:TemplateField HeaderText="Booking Date">
                                            <ItemTemplate>
                                                <asp:Label ID="lblCREATEDDATE" runat="server" Text='<%#Eval("CREATEDDATE")%>'></asp:Label>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:TextBox ID="txtCREATEDDATE" MaxLength="3" runat="server" Width="60px" Text='<%#Eval("CREATEDDATE")%>'></asp:TextBox>
                                            </EditItemTemplate>
                                        </asp:TemplateField>



                                        
                                      
                                        <asp:TemplateField HeaderText ="Status">
                                            <ItemTemplate>
                                                <asp:Label ID="lblsts" runat="server" Text='<%#Eval("BOOKINGSTATUS")%>'></asp:Label>
                                            </ItemTemplate>

                                        </asp:TemplateField>
                                    

                                        <asp:TemplateField HeaderText="Remark" Visible="false">
                                            <EditItemTemplate>
                                                <asp:TextBox ID="txtRemark" runat="server" TextMode="MultiLine"></asp:TextBox>
                                            </EditItemTemplate>
                                        </asp:TemplateField>
                                    
                                    </Columns>
                                    <HeaderStyle CssClass="HeaderStyle" />
                                </asp:GridView>
                                <b>
                                    <asp:Label ID="lblUpdatePax" runat="server" Visible="false"></asp:Label></b>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        <asp:UpdateProgress ID="updateprogress1" runat="server" AssociatedUpdatePanelID="UpdatePanel2">
                            <ProgressTemplate>
                                <div style="position: fixed; top: 0px; bottom: 0px; left: 0px; right: 0px; overflow: hidden; padding: 0; margin: 0; background-color: #000; filter: alpha(opacity=50); opacity: 0.5; z-index: 1000;">
                                </div>
                                <div style="position: fixed; top: 30%; left: 43%; padding: 10px; width: 20%; text-align: center; z-index: 1001; background-color: #fff; border: solid 1px #000; font-size: 12px; font-weight: bold; color: #000000">
                                    Please Wait....<br />
                                    <br />
                                    <img alt="loading" src="<%= ResolveUrl("~/images/loadingAnim.gif")%>" />
                                    <br />
                                </div>
                            </ProgressTemplate>
                        </asp:UpdateProgress>
                    </td>
                </tr>

            </table>

             <div>
                &nbsp;
            </div>
            <table width="90%" class="tbltbl" border="0" cellpadding="10" cellspacing="10">
                <tr>
                    <td align="left" style="font-weight: bold; font-size: 14px;">
                    </td>
                </tr>
                <tr>
                    <div class="form-group">
                                    <label for="exampleInputEmail1"> PNR</label>
                                    <asp:TextBox ID="txt_PNR" runat="server"  Width="40%" MaxLength="60"></asp:TextBox>                                                                   
                                </div>
                </tr>

                <tr>
                    <label for="exampleInputEmail1"> Ticket No</label>
                     <asp:TextBox ID="Txt_tktno" runat="server"   Width="40%" MaxLength="60"></asp:TextBox>
                </tr>


                <tr>
                    <asp:Button ID="Button1" type="button" runat="server" Text="Update"  CssClass="button buttonBlue" OnClick="btn_update_Click" />  
                </tr>
            </table>

       
        </div>
    </form> 
</body>
</html>
