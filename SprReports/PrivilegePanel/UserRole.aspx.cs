﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

public partial class Duplicate : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        
        if (!this.IsPostBack)
            {
                this.BindGridview();
                BindRole();
            }
    }

    public void BindRole()
    {
        string constr = ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString;
        SqlConnection con = new SqlConnection(constr);

        SqlCommand cmd = new SqlCommand("SELECT Role FROM Role_tab");

        cmd.CommandType = CommandType.Text;
        cmd.Connection = con;
        con.Open();
        Role.DataSource = cmd.ExecuteReader();

        Role.DataValueField = "Role";


        Role.DataBind();

        con.Close();
        Role.Items.Insert(0, new ListItem("--Select RoleList--", "0"));




    }


    protected void Submit_Click(object sender, EventArgs e)
    {
        
            Pro bo = new Pro();

            string result = "";
            bo.U_type = usertypetxt.Value;
            bo.U_role = Role.SelectedItem.ToString();
        
            Bal bl = new Bal();

            try
            {
                result = bl.User_typeinsert(bo);
                if (result == "Insert")
                {
                  
                    Label1.Text = "Data successfully inserted in database";
                    usertypetxt.Value = "";
                    Role.SelectedIndex = 0; 
                    BindGridview();
                    
                }
                else
                {


                    Label1.Text = "Data Already Exist Try again...";
                    usertypetxt.Value = "";
                    Role.SelectedIndex = 0;
                    BindGridview();

                }
            }

            catch (Exception ex)
            {
                throw ex;
            }
           finally
            {
                bo = null;
                bl = null;
            }
            Role.SelectedIndex = 0;
        
}

    private static DataTable GetData(string query)
    {

        string constr = ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString;
        SqlConnection con = new SqlConnection(constr);
        con.Open();

        SqlCommand cmd = new SqlCommand();
        cmd.CommandText = query;

        SqlDataAdapter sda = new SqlDataAdapter();

        cmd.Connection = con;
        sda.SelectCommand = cmd;
        DataTable dt = new DataTable();

        sda.Fill(dt);
        con.Close();

        return dt;
    }
    protected void BindGridview()
    {
        GridView1.DataSource = GetData("select * from Duplicate_Role");
        GridView1.DataBind();

    }
    protected void OnRowCancelingEdit(object sender, EventArgs e)
    {
        GridView1.EditIndex = -1;
        this.BindGridview();
    }

    protected void OnRowEditing(object sender, GridViewEditEventArgs e)
    {
        GridView1.EditIndex = e.NewEditIndex;
        this.BindGridview();
    }

    protected void OnRowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        
        string sno = GridView1.DataKeys[e.RowIndex].Values[0].ToString();
        string constr = ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString;
        using (SqlConnection con = new SqlConnection(constr))
        {
            using (SqlCommand cmd = new SqlCommand("DeleteSp2"))
            {
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@sno", sno);
                cmd.Connection = con;
                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();
            }
        }
        this.BindGridview();
        BindRole();
    }


    protected void OnRowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        GridViewRow row = GridView1.Rows[e.RowIndex];
        string sno = GridView1.DataKeys[e.RowIndex].Values[0].ToString();

        string role = (row.FindControl("txtRole") as TextBox).Text;
        string constr = ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString;
        using (SqlConnection con = new SqlConnection(constr))
        {
            using (SqlCommand cmd = new SqlCommand("UpdateSp2"))
            {
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@sno", sno);
                cmd.Parameters.AddWithValue("@Role", role);
                cmd.Connection = con;
                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();
            }
        }
        GridView1.EditIndex = -1;
        this.BindGridview();
    }

}