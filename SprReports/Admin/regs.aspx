﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterAfterLogin.master" AutoEventWireup="false"
    CodeFile="regs.aspx.vb" Inherits="regs" %>


<%--Title="" Language="VB" MasterPageFile="~/MasterAfterLogin.master" AutoEventWireup="false"
    CodeFile="DomAirlineMarkup.aspx.vb" EnableViewStateMac="false" Inherits="Reports_Admin_DomAirlineMarkup"--%>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <style type="text/css">
        .clear1 {
    clear: both;
    padding:4px;
}
    .rthead {
    font-size: 25px;
    color: #fff;
    background-color: #0e4ca2;
    width: 100%;
    text-align: center;
    padding: 10px;
}
    .w70{
        width:70% !important;
    }

        .rtmainbgs {
            background: #fff;
            border-radius: 4px;
            margin-top: 20px;
            margin-bottom: 20px;
        }

        .rtbg {
            background-color: #0e4ca2;
        }

        .form-controlrt {
            display: block;
            width: 100%;
            height: 40px !important;
            padding: 6px 12px;
            font-size: 14px;
            line-height: 1.42857143;
            color: #555;
            background-color: #fff;
            background-image: none;
            border: 1px solid #ccc;
            border-radius: 0px;
            -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
            box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
            -webkit-transition: border-color ease-in-out .15s, -webkit-box-shadow ease-in-out .15s;
            -o-transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
            transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
        }

        }
    </style>
    <link rel="stylesheet" href="chosen/chosen.css" />

    <script src="chosen/jquery-1.6.1.min.js" type="text/javascript"></script>

    <script src="chosen/chosen.jquery.js" type="text/javascript"></script>
     <link href="CSS/jquery-ui-1.8.8.custom.css" rel="stylesheet" />


    <script type="text/javascript">
        $(document).ready(function () {
            $(".chzn-select").chosen();
            $(".chzn-select-deselect").chosen({ allow_single_deselect: true });
        });
    </script>

    <script type="text/javascript">
        function RefreshCaptcha() {
            var img = document.getElementById("imgCaptcha");
            img.src = "../../CAPTCHA.ashx?query=" + Math.random();
        }
    </script>

    <script language="javascript" type="text/javascript">
        function validateSearch() {

            if (document.getElementById("ctl00_ContentPlaceHolder1_Fname_txt").value == "") {
                alert('Specify First Name');
                document.getElementById("ctl00_ContentPlaceHolder1_Fname_txt").focus();
                return false;
            }
            if (document.getElementById("ctl00_ContentPlaceHolder1_Lname_txt").value == "") {
                alert('Specify Last Name');
                document.getElementById("ctl00_ContentPlaceHolder1_Lname_txt").focus();
                return false;
            }
            if (document.getElementById("ctl00_ContentPlaceHolder1_Add_txt").value == "") {
                alert('Specify Address');
                document.getElementById("ctl00_ContentPlaceHolder1_Add_txt").focus();
                return false;
            }

            if (document.getElementById("ctl00_ContentPlaceHolder1_ddl_country").value == "India") {
                if (document.getElementById("ctl00_ContentPlaceHolder1_ddl_state").value == "--Select State--") {
                    alert('Please Select State');
                    document.getElementById("ctl00_ContentPlaceHolder1_ddl_state").focus();
                    return false;
                }
                if (document.getElementById("ctl00_ContentPlaceHolder1_ddl_city").value == "") {
                    alert('Please Select City');
                    document.getElementById("ctl00_ContentPlaceHolder1_ddl_city").focus();
                    return false;
                }

            }
            else {
                if (document.getElementById("ctl00_ContentPlaceHolder1_Coun_txt").value == "") {
                    alert('Specify Country Name');
                    document.getElementById("ctl00_ContentPlaceHolder1_Coun_txt").focus();
                    return false;
                }
                if (document.getElementById("ctl00_ContentPlaceHolder1_Stat_txt").value == "") {
                    alert('Specify State Name');
                    document.getElementById("ctl00_ContentPlaceHolder1_Stat_txt").focus();
                    return false;
                }
                if (document.getElementById("ctl00_ContentPlaceHolder1_City_txt").value == "") {
                    alert('Specify City Name');
                    document.getElementById("ctl00_ContentPlaceHolder1_City_txt").focus();
                    return false;
                }
            }
            if (document.getElementById("ctl00_ContentPlaceHolder1_Pin_txt").value == "")
            {
                alert('Specify Pincode');
                document.getElementById("ctl00_ContentPlaceHolder1_Pin_txt").focus();
                return false;

            }
            if (document.getElementById("ctl00_ContentPlaceHolder1_Mob_txt").value == "") {
                alert('Specify Mobile Number');
                document.getElementById("ctl00_ContentPlaceHolder1_Mob_txt").focus();
                return false;
            }
            if (document.getElementById("ctl00_ContentPlaceHolder1_Email_txt").value == "") {
                alert('Specify EmailID');
                document.getElementById("ctl00_ContentPlaceHolder1_Email_txt").focus();
                return false;
            }

            var emailPat = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
            var emailid = document.getElementById("ctl00_ContentPlaceHolder1_Email_txt").value;
            var matchArray = emailid.match(emailPat);
            if (matchArray == null) {
                alert("Your email address seems incorrect. Please try again.");
                document.getElementById("ctl00_ContentPlaceHolder1_Email_txt").focus();
                return false;
            }

            if (document.getElementById("ctl00_ContentPlaceHolder1_Agn_txt").value == "") {
                alert('Specify Agency Name');
                document.getElementById("ctl00_ContentPlaceHolder1_Agn_txt").focus();
                return false;
            }
            if (document.getElementById("ctl00_ContentPlaceHolder1_TextBox_NameOnPard").value == "") {
                alert('Specify Name on Pan Card');
                document.getElementById("ctl00_ContentPlaceHolder1_TextBox_NameOnPard").focus();
                return false;
            }
            if (document.getElementById("ctl00_ContentPlaceHolder1_Pan_txt").value == "") {
                alert('Specify Pan No');
                document.getElementById("ctl00_ContentPlaceHolder1_Pan_txt").focus();
                return false;
            }
            if (document.getElementById("ctl00_ContentPlaceHolder1_Ans_txt").value == "") {
                alert('Specify Answer');
                document.getElementById("ctl00_ContentPlaceHolder1_Ans_txt").focus();
                return false;
            }


            if (document.getElementById("ctl00_ContentPlaceHolder1_TxtUserId").value == "") {
                alert('Specify Userid');
                document.getElementById("ctl00_ContentPlaceHolder1_TxtUserId").focus();
                return false;
            }

            if (document.getElementById("ctl00_ContentPlaceHolder1_Pass_text").value == "") {
                alert('Specify Password');
                document.getElementById("ctl00_ContentPlaceHolder1_Pass_text").focus();
                return false;
            }
            else {
                var regex = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[#$@!%&*?])[A-Za-z\d#$@!%&*?]{8,16}$/;
                if (!regex.test(document.getElementById("ctl00_ContentPlaceHolder1_Pass_text").value)) {
                    alert("Password must contain:8 To 16 characters atleast 1 UpperCase Alphabet, 1 LowerCase Alphabet, 1 Number and 1 Special Character'");
                    document.getElementById("ctl00_ContentPlaceHolder1_Pass_text").focus();
                    return false;
                }


            if (document.getElementById("ctl00_ContentPlaceHolder1_cpass_txt").value == "") {
                alert('Specify Confirm Password');
                document.getElementById("ctl00_ContentPlaceHolder1_cpass_txt").focus();
                return false;
            }

            if (document.getElementById("ctl00_ContentPlaceHolder1_cpass_txt").value != "") {
                
                if (document.getElementById("ctl00_ContentPlaceHolder1_Pass_text").value != document.getElementById("ctl00_ContentPlaceHolder1_cpass_txt").value) {
                    alert("Confirm Password is same as Password");
                    document.getElementById("ctl00_ContentPlaceHolder1_cpass_txt").focus();
                    return false;
                }

                if (confirm("Are you sure!"))
                    return true;
                return false;

            }

            //            alert_message = "";
            //            formObj = document.forms["flight"];
            //            var now = new Date();
            //            if (document.getElementById("Fname_txt").value == "") alert_message += " - Specify First Name\n";
            //            if (formObj.Lname_txt.value == "") alert_message += " - Specify Last Name\n";
            //            if (formObj.Add_txt.value == "") alert_message += " - Specify Address\n";
            //            if (formObj.City_txt.value == "") alert_message += " - Specify City\n";

            //            if (formObj.Mob_txt.value == "") alert_message += " - Specify Mobile Number\n";
            //            if (formObj.Email_txt.value == "") alert_message += " - Specify E-Mail\n";
            //            if (formObj.Agn_txt.value == "") alert_message += " - Specify Agency Name\n";

            //            if (formObj.Pan_txt.value == "") alert_message += " - Specify Pan Number\n";
            //            if (formObj.Ans_txt.value == "") alert_message += " - Specify Answer\n";
            //            //if(formObj.uid_txt.value == "" )    alert_message+=" - Specify User ID\n";

            //            if (formObj.Pass_txt.value == "") alert_message += " - Specify Password\n";
            //            if (formObj.cpass_txt.value == "") alert_message += " - Specify Confirm Password\n";

            //            if (alert_message != "") {
            //                alert("Please fix the following problems: \n" + alert_message);
            //                return false;
            //            }
            //            if (formObj.Pass_txt.value != "") {
            //                if (formObj.Pass_txt.value != formObj.cpass_txt.value) {
            //                    alert("Confirm Password is same as Password");
            //                    return false;
            //                }
            //            }

            //            if (formObj.Email_txt.value != "") {
            //                var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/

            //                if (!formObj.Email_txt.value.match(re)) {
            //                    alert('Your email address is not in a valid format.\n Some valid formats are:\nsomeone@somewhere.com\nsome_one@some.where.net')

            //                    var nogood = 'yes';
            //                    return false;
            //                }
            //            }
            //            return true;

        }

        function phone_vali() {
            if ((event.keyCode > 47 && event.keyCode < 58) || (event.keyCode == 32) || (event.keyCode == 45))
                event.returnValue = true;
            else
                event.returnValue = false;
        }
        function vali() {
            if ((event.keyCode > 64 && event.keyCode < 91) || (event.keyCode > 96 && event.keyCode < 123) || (event.keyCode == 32) || (event.keyCode == 45))
                event.returnValue = true;
            else
                event.returnValue = false;
        }

        function vali1() {
            if ((event.keyCode > 64 && event.keyCode < 91) || (event.keyCode > 96 && event.keyCode < 123) || (event.keyCode == 32) || (event.keyCode > 47 && event.keyCode < 58) || (event.keyCode == 32))
                event.returnValue = true;
            else
                event.returnValue = false;
        }

        function getKeyCode(e) {
            if (window.event)
                return window.event.keyCode;
            else if (e)
                return e.which;
            else
                return null;
        }
        function keyRestrict(e, validchars) {
            var key = '', keychar = '';
            key = getKeyCode(e);
            if (key == null) return true;
            keychar = String.fromCharCode(key);
            keychar = keychar.toLowerCase();
            validchars = validchars.toLowerCase();
            if (validchars.indexOf(keychar) != -1)
                return true;
            if (key == null || key == 0 || key == 8 || key == 9 || key == 13 || key == 27)
                return true;
            return false;
        }

    </script>
    <%--<script>

    function HideTextBox(ddlId) {
        var ControlName = document.getElementById(ddlId.id);

        if (ControlName.value == "Other")  //it depends on which value Selection do u want to hide or show your textbox
        {
            document.getElementById('ctl00_ContentPlaceHolder1_ddl_city').style.display = 'none';
            document.getElementById('ctl00_ContentPlaceHolder1_City_txt').style.display = 'block';

        }
//        else {
//            document.getElementById('MyTextBox').style.display = '';

//        }
    }--%>


    <%--<asp:ScriptManager ID="ScriptManager1" runat="server" EnablePageMethods="true">
                    </asp:ScriptManager>--%>
    <%-- <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>--%>
   <div class="col-md-12">
        <div class="" style="line-height: 25px; padding-bottom:10px;margin-left: 30px; ">
            <div class="row rtmainbgs">
                <div class="rthead"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>Register Now</div>
              


                <div class="w100" id="table_reg" runat="server"
                    visible="true" style="margin-top: 0px; padding-bottom: 20px;">
                      <div class="row">
                    <div class="col-md-12">
                        <div class="">
                            <h4>Personal Information</h4>
                        </div>
                    </div>
                <div class="col-lg-12 col-sm-12 col-xs-12">
                    <div class="heading w100">
                       
                            <div align="center">
                                <asp:Label ID="lbl_msg" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="20px"
                                    ForeColor="#FF3300"></asp:Label>
                            </div>
                        

                                <div class="clear1"></div>


                                <div class="w100">
                                    <div class="col-lg-2 col-sm-2 col-xs-3">Title:<span style="color: #990000" class="lft">*</span>

                                    </div>
                                    <div class="col-lg-2 col-sm-2 col-xs-9">
                                        <asp:DropDownList ID="tit_drop" runat="server" CssClass="lft w70 form-controlrt ">
                                            <asp:ListItem Value="Mr.">Mr.</asp:ListItem>
                                            <asp:ListItem Value="Ms.">Ms.</asp:ListItem>
                                            <asp:ListItem Value="Mrs.">Mrs.</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>

                                     <div class="col-lg-2 col-sm-2 col-xs-3">
                                First Name:<span style="color: #990000">*</span>
                            </div>
                            <div class="col-lg-2 col-sm-2 col-xs-9">
                                <asp:TextBox ID="Fname_txt" CssClass="psb_dd form-controlrt" runat="server" Style="position: static"
                                    onkeypress="return keyRestrict(event,' abcdefghijklmnopqrstuvwxyz');"  MaxLength="50"></asp:TextBox>
                            </div>


                                    <div class="col-lg-2 col-sm-2 col-xs-3">
                                        Last Name:<span style="color: #990000">*</span>
                                    </div>
                                    <div class="col-lg-2  col-sm-2 col-xs-9">
                                        <asp:TextBox ID="Lname_txt" CssClass="psb_dd form-controlrt" runat="server" Style="position: static;"
                                            onkeypress="return keyRestrict(event,'abcdefghijklmnopqrstuvwxyz');" MaxLength="50"></asp:TextBox>
                                    </div>
                                   
                                    <div class="clear1"></div>

                                     <div class="col-lg-2 col-sm-2 col-xs-3">
                                Address:<span style="color: #990000">*</span>
                            </div>

                            <div class="col-lg-2 col-sm-2 col-xs-9">
                                <asp:TextBox ID="Add_txt" runat="server" CssClass="form-controlrt" Style="height: 50px;"></asp:TextBox>
                            </div>
                                  
                                        <div class="col-lg-2 col-sm-2 col-xs-3">
                                            Country:
                                        </div>
                                        <div class="col-lg-2 col-sm-2 col-xs-9">
                                            <asp:DropDownList ID="ddl_country" runat="server" CssClass="form-controlrt" AutoPostBack="True">
                                                <asp:ListItem Selected="True" Value="India">India</asp:ListItem>
                                                <asp:ListItem Value="Other">Other</asp:ListItem>
                                            </asp:DropDownList>
                                            <asp:TextBox ID="Coun_txt" CssClass="psb_dd form-controlrt " runat="server" MaxLength="30" Style="position: static"
                                                onkeypress="return keyRestrict(event,'abcdefghijklmnopqrstuvwxyz');" Visible="false"></asp:TextBox>
                                        </div>
          

                                  
                                    
                            <div class="col-lg-2 col-sm-2 col-xs-3">
                                State*:
                            </div>
                            <div class="col-lg-2 col-sm-2 col-xs-9">
                                <asp:DropDownList ID="ddl_state" runat="server" AutoPostBack="True" CssClass=" form-controlrt">
                                </asp:DropDownList>
                                <asp:TextBox ID="Stat_txt" CssClass="psb_dd form-controlrt" runat="server" Style="position: static"
                                    onkeypress="return vali();" Visible="false"></asp:TextBox>
                            </div>
                                       <div class="clear1"></div>
                           <div class="col-lg-2 col-sm-2 col-xs-3">
                                            City:<span style="color: #990000">*</span>
                                        </div>
                                        <div class="col-lg-2 col-sm-2 col-xs-9">
                                           <%-- <asp:DropDownList ID="ddl_city" runat="server" CssClass="form-controlrt">
                                            </asp:DropDownList>
                                            <asp:TextBox ID="City_txt" CssClass="psb_dd form-controlrt" runat="server" Style="position: static"
                                                onkeypress="return keyRestrict(event,'abcdefghijklmnopqrstuvwxyz');" MaxLength="50" Visible="false"></asp:TextBox>--%>
                                     <input type="text" id="ddl_city" runat="server" class="psb_dd form-controlrt"/>
                                             <asp:TextBox ID="Other_City" CssClass="psb_dd form-controlrt" runat="server" Style="position: static"
                                    onkeypress="return vali();" Visible="false"></asp:TextBox>
                                        </div>
                             <div class="col-lg-2 col-sm-2 col-xs-3">
                                Area:
                            </div>
                            <div class="col-lg-2 col-sm-2 col-xs-9">
                                <asp:TextBox ID="TextBox_Area" CssClass="psb_dd form-controlrt" runat="server" MaxLength="30" Style="position: static"></asp:TextBox>
                                  
                            </div>
                                  
                            
                            <div class="col-lg-2 col-sm-2 col-xs-3">
                                Pincode*:
                            </div>
                            <div class="col-lg-2 col-sm-2 col-xs-9">
                                <asp:TextBox ID="Pin_txt" CssClass="psb_dd form-controlrt" runat="server" Style="position: static"
                                    onkeypress="return keyRestrict(event,'1234567890');" MaxLength="8"></asp:TextBox>
                            </div>
 <div class="clear1"><hr /> </div>
                                   
                                </div>
                          
                        <div class="w100">
                            <div class="">
                                <h4>Contact Information</h4>
                            </div>
                           
                                <div class="col-lg-2 col-sm-2 col-xs-3">
                                    Phone:
                                </div>
                                <div class="col-lg-2 col-sm-2 col-xs-9">
                                    <asp:TextBox ID="Ph_txt" CssClass="psb_dd form-controlrt" runat="server" Style="position: static"
                                        onkeypress="return keyRestrict(event,'0123456789');" MaxLength="12"></asp:TextBox>

                                </div>

                              <div class="col-lg-2 col-sm-2 col-xs-3">
                                    Mobile:<span style="color: #990000">*</span>
                                </div>
                                <div class="col-lg-2  col-sm-2 col-xs-9">
                                    <asp:TextBox ID="Mob_txt" CssClass="psb_dd form-controlrt" runat="server" Style="position: static"
                                        onkeypress="return keyRestrict(event,'0123456789');" MaxLength="10"></asp:TextBox>
                                </div>

                            
                                
                                <div class="col-lg-2 col-sm-2 col-xs-3">
                                    Email Id:<span style="color: #990000">*</span>
                                </div>
                                <div class="col-lg-2  col-sm-2 col-xs-9">
                                    <asp:TextBox ID="Email_txt" CssClass="psb_dd form-controlrt" runat="server" Style="position: static" MaxLength="50"></asp:TextBox>
                                </div>

                            <div class="clear1"></div>
                               
                            
                                <div class="col-lg-2 col-sm-2 col-xs-3">
                                    Email 2:
                                </div>
                                <div class="col-lg-2  col-sm-2 col-xs-9">
                                    <asp:TextBox ID="Aemail_txt" CssClass="psb_dd form-controlrt" runat="server" Style="position: static" MaxLength="50"></asp:TextBox>
                                </div>
                                <div class="col-lg-2 col-sm-2 col-xs-3">
                                    Fax No.:
                                </div>
                                <div class="col-lg-2 col-sm-2 col-xs-9">
                                    <asp:TextBox ID="Fax_txt" CssClass="psb_dd form-controlrt" runat="server" Style="position: static" MaxLength="40"></asp:TextBox>
                                </div>
                          
                           
                               <div class="clear1"><hr /> </div>
                               
                            </div>

                        <div class="w100">
                            <div class="">
                                <h4>Agency Information</h4>
                            </div>
                            
                                <div class="col-lg-2 col-sm-2 col-xs-3">
                                    Agency Name:<span style="color: #990000">*</span>
                                </div>
                                <div class="col-lg-2 col-sm-2 col-xs-9">
                                    <asp:TextBox ID="Agn_txt" CssClass="psb_dd form-controlrt" runat="server" MaxLength="50" onkeypress="return keyRestrict(event,' abcdefghijklmnopqrstuvwxyz0123456789');"></asp:TextBox>
                                </div>

                            
                                <div class="col-lg-2 col-sm-2 col-xs-3">
                                 
                                    Agent Type
                                </div>
                                <div class="col-lg-2 col-sm-2 col-xs-9">
                                    <asp:DropDownList ID="Stat_drop" CssClass="form-controlrt" runat="server">
                                            <asp:ListItem Value="TA" Selected="True">Travel Agent</asp:ListItem>
                                            <asp:ListItem Value="DI">Stockist</asp:ListItem>
                                        </asp:DropDownList>
                                    <asp:TextBox ID="Web_txt" CssClass="psb_dd form-controlrt" runat="server" Style="position: static;display:none;"></asp:TextBox>
                                </div>
                             <div class="col-lg-2 col-sm-2 col-xs-3">
                                    Stax No:
                                </div>
                                <div class="col-lg-2  col-sm-2 col-xs-9">
                                    <asp:TextBox ID="Stax_txt" CssClass="psb_dd form-controlrt" runat="server" Style="position: static"></asp:TextBox>
                                </div>

                            <div class="clear1"></div>
                               
                                 <div class="col-lg-2 col-sm-2 col-xs-3">
                                    Name On Pan Card:<span style="color: #990000">*</span>
                                </div>
                                <div class="col-lg-2 col-sm-2 col-xs-9">
                                    <asp:TextBox ID="TextBox_NameOnPard" CssClass="psb_dd form-controlrt" runat="server" Style="position: static"></asp:TextBox>
                                </div>
                                
                                <div class="col-lg-2 col-sm-2 col-xs-3">
                                    Pan No:<span style="color: #990000">*</span>
                                </div>
                                <div class="col-lg-2 col-sm-2 col-xs-9">
                                    <asp:TextBox ID="Pan_txt" CssClass="psb_dd form-controlrt" runat="server" Style="position: static"></asp:TextBox>
                                </div>
                               
                                <div class="col-lg-2 col-sm-2 col-xs-3">
                                    Pan Image*:  
                                </div>
                                <div class="col-lg-2 col-sm-2 col-xs-9">
                                    <asp:FileUpload ID="fld_pan" runat="server" CssClass="psb_dd" Height="22px" />                                    
                                    <div class="" style="font-size:11px;color:#0e4faa;font-weight:bold">
                                        ( Pancard image must be in JPG formate )
                                    </div>
                               </div>

                            <div class="clear1"></div>

                                <div class="col-lg-2 col-sm-2 col-xs-3">
                                    Remark:
                                </div>
                                <div class="col-lg-2 col-sm-2 col-xs-9">
                                    <asp:TextBox ID="Rem_txt" CssClass="psb_dd form-controlrt" runat="server" Style="position: static"></asp:TextBox>
                                </div>
                               
                               
                                <div class="col-lg-2 col-sm-2 col-xs-3">
                                    Ref. By :
                                </div>
                                <div class="col-lg-2 col-sm-2 col-xs-9">
                                    <asp:DropDownList ID="Sales_DDL" runat="server" CssClass="form-controlrt">
                                    </asp:DropDownList>
                                </div>
                               

                                <div class="col-lg-2 col-sm-2 col-xs-3">
                                    Upload Logo : 
                                </div>
                                <div class="col-lg-2 col-sm-2 col-xs-9">
                                    <asp:FileUpload ID="fld_1" runat="server" CssClass="psb_dd" />
                                    <div style="font-size:11px;color:#0e4faa;font-weight:bold"> ( Image must be in JPG formate and
                                                        Size should be (90*70) pixels)</div>

                                </div>

                            <div class="clear1"></div>
                             <div class="col-lg-2 col-sm-2 col-xs-6">
                                    Security Question:<span style="color: #990000">*</span>
                                </div>
                                <div class="col-lg-3 col-sm-1 col-xs-6">
                                    <asp:DropDownList ID="SecQ_drop" CssClass="form-controlrt" runat="server">
                                        <asp:ListItem Value="What is Your Pet Name?">Mr.What is Your Pet Name?</asp:ListItem>
                                        <asp:ListItem Value="What is your Favourite Color?">What is your Favourite Color?</asp:ListItem>
                                        <asp:ListItem Value="What is Your Date of Birth">What is Your Date of Birth</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                               
                                <div class="col-lg-2 col-sm-2 col-xs-6">
                                    Security Answer:<span style="color: #990000">*</span>&nbsp;
                                </div>
                                <div class="col-lg-2 col-sm-2 col-xs-6">
                                    <asp:TextBox ID="Ans_txt" CssClass="psb_dd form-controlrt" runat="server" Style="position: static"></asp:TextBox>
                                </div>
                                
                        <div class="clear1"><hr /></div>

                        
                        </div>
                        <div class="w100">
                            <div class="">
                                <h4>Authentication Information</h4>
                            </div>

                           
                                <div class="col-lg-2 col-sm-2 col-xs-3">
                                    User Id:<span style="color: #990000">*</span>
                                </div>
                                <div class="col-lg-2 col-sm-2 col-xs-9">
                                    <asp:TextBox ID="TxtUserId" runat="server" Style="position: static" MaxLength="20" CssClass="psb_dd form-controlrt" oncopy="return false" onpaste="return false"  onkeypress="return keyRestrict(event,'abcdefghijklmnopqrstuvwxyz1234567890');"></asp:TextBox>
                                    <%--<asp:TextBox ID="TxtUserId" runat="server" Style="position: static" MaxLength="50" CssClass="psb_dd form-controlrt" onkeypress="return keyRestrict(event,'abcdefghijklmnopqrstuvwxyz1234567890');"></asp:TextBox>--%>
                                </div>
                                <div class="col-lg-2 col-sm-1 col-xs-3">
                                    Password:<span style="color: #990000">*</span>
                                </div>
                                <div class="col-lg-2 col-sm-2 col-xs-9">
                                    <asp:TextBox ID="Pass_text" runat="server" Style="position: static"
                                        TextMode="Password" MaxLength="16" CssClass="psb_dd form-controlrt"></asp:TextBox>
                                    <div style="font-size:11px;color:#0e4faa;font-weight:bold">
                                    Eg. abc@123(not more than 16 charecter)</div>
                                </div>

                                <div class="col-lg-2 col-sm-1 col-xs-3">
                                    Confirm Password:<span style="color: #990000">*</span>&nbsp;
                                </div>
                                <div class="col-lg-2 col-sm-2 col-xs-9">
                                    <asp:TextBox ID="cpass_txt" CssClass="psb_dd form-controlrt" runat="server" Style="position: static"
                                         TextMode="Password" MaxLength="16"></asp:TextBox>
                                </div>
                                <div class="clear1"></div>
                                <div class="col-lg-2 col-sm-2 col-xs-3">Captcha Information:*</div>
                                <div class="col-lg-2 col-sm-1 col-xs-3">
                                  <%--  <a href="../../CAPTCHA.ashx">../../CAPTCHA.ashx</a>--%>
                                    <img src="../../CAPTCHA.ashx" id="imgCaptcha" />
                                    &nbsp;<a onclick="javascript:RefreshCaptcha();" style="cursor: pointer"><img src="../../Images/refresh.png" /></a>
                                </div>

                                <div class="col-lg-2 col-sm-2 col-xs-3">Enter Text from Image*</div>
                                <div class="col-lg-2 col-sm-2 col-xs-9">
                                    <asp:TextBox ID="TextBox1" CssClass="form-controlrt" runat="server"></asp:TextBox>
                                </div>
                            
                            <div class="col-lg-2 col-sm-2 col-xs-6" >
                                <asp:Button ID="submit" Width="150" runat="server" Text="Submit" OnClientClick="return validateSearch()"
                                    CssClass="button buttonBlue" />
                            </div>
 
                        </div>
                        <div class="clear1">
                    </div>
 
                </div>

                    </div>
                
            </div>
            
        </div>
                <div id="table_Message" runat="server" visible="false">

                <div class="autoss">
                    Thanks You....!
                </div>

                <div class="w80 auto">
                    <div class="w100">


                        <div class="regss" style="align-items:center;">
                            <b> User Id is : - </b>
                            <%=CID%>  <br />Agent successfully registered.<br />
                            <%=CID%> is still inactive.  <br />
                        </div>
                        <div class="clear1"></div>

                        <div class="regss">
                            <b>Please activate agent user id.</b>
                        </div>


                    </div>

                </div>

            </div>
    </div>
<script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-1.7.1.min.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-ui-1.8.8.custom.min.js")%>"></script>
    <%--<script type="text/javascript">
        var UrlBase = '<%=ResolveUrl("~/") %>';
        var autoCity = UrlBase + "AutoComplete.asmx/GETCITYSTATE";
        $("#ctl00_ContentPlaceHolder1_ddl_city").autocomplete({
            source: function (request, response) {

                //if ($("#ctl00_ContentPlaceHolder1_ddl_state").val() == "") {
                //    $("#ctl00_ContentPlaceHolder1_ddl_state").focus();
                //    alert("Please Select state");
                //    return false;
               // }
                $.ajax({
                    url: autoCity,
                    data: "{ 'INPUT': '" + $("#ctl00_ContentPlaceHolder1_ddl_state").val() + "','SEARCH': '" + request.term + "' }",
                    dataType: "json", type: "POST",
                    contentType: "application/json; charset=utf-8",

                    success: function (data) {

                        response($.map(data.d, function (item) {
                            return { label: item, value: item, id: $("#ctl00_ContentPlaceHolder1_ddl_state").val() }
                        }))
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {

                        alert(textStatus);
                    }
                })
            },
            autoFocus: true,
            minLength: 3,
            select: function (event, ui) {

            }
        });

      </script>--%>
    
            <%--<script type="text/javascript">
                var UrlBase = '<%=ResolveUrl("~/") %>';
                var autoCity = UrlBase + "AutoComplete.asmx/GETCITYSTATE";
                $("#ctl00_ContentPlaceHolder1_ddl_city").autocomplete({
                    source: function (request, response) {

                        ////if ($("#ctl00_ContentPlaceHolder1_ddl_state").val() == "") {
                        ////    $("#ctl00_ContentPlaceHolder1_ddl_state").focus();
                        ////    alert("Please Select state");
                        ////    return false;
                        //}
                        $.ajax({
                            url: autoCity,
                            data: "{ 'INPUT': '" + $("#ctl00_ContentPlaceHolder1_ddl_state").val() + "','SEARCH': '" + request.term + "' }",
                            dataType: "json", type: "POST",
                            contentType: "application/json; charset=utf-8",

                            success: function (data) {

                                if (data.d.length > 0) {
                                    response($.map(data.d, function (item) {
                                        return { label: item, value: item, id: $("#ctl00_ContentPlaceHolder1_ddl_state").val() }
                                    }))

                                }
                                else {
                                    response([{ label: 'City Not Found of Selected State', val: -1 }]);
                                }
                                //response($.map(data.d, function (item) {
                                //    return { label: item, value: item, id: $("#ctl00_ContentPlaceHolder1_ddl_state").val() }
                                //}))
                            },
                            error: function (XMLHttpRequest, textStatus, errorThrown) {

                                alert(textStatus);
                            }
                        })
                    },
                    autoFocus: true,
                    minLength: 3,
                    select: function (event, ui) {
                        if (ui.item.val == -1) {
                            $(this).val("");
                            return false;
                        }
                    }
                });

      </script>--%>
            <script type="text/javascript">
                var UrlBase = '<%=ResolveUrl("~/") %>';
                var autoCity = UrlBase + "AutoComplete.asmx/GETCITYSTATE";
                $("#ctl00_ContentPlaceHolder1_ddl_city").autocomplete({
                    source: function (request, response) {

                        //if ($("#ctl00_ContentPlaceHolder1_ddl_city").val()!=data.d.item) {
                        //    $("#ctl00_ContentPlaceHolder1_ddl_city").focus();
                        //    alert("Please Select appropriate city");
                        //    return false;
                        //}
                        $.ajax({
                            url: autoCity,
                            data: "{ 'INPUT': '" + $("#ctl00_ContentPlaceHolder1_ddl_state").val() + "','SEARCH': '" + request.term + "' }",
                            dataType: "json", type: "POST",
                            contentType: "application/json; charset=utf-8",

                            success: function (data) {

                                if (data.d.length > 0) {
                                    response($.map(data.d, function (item) {
                                        return { label: item, value: item, id: $("#ctl00_ContentPlaceHolder1_ddl_state").val() }
                                    }))

                                }
                                else {
                                    response([{ label: 'City Not Found', val: -1 }]);
                                }
                                //response($.map(data.d, function (item) {
                                //    return { label: item, value: item, id: $("#ctl00_ContentPlaceHolder1_ddl_state").val() }
                                //}))
                            },
                            error: function (XMLHttpRequest, textStatus, errorThrown) {

                                alert(textStatus);
                            }
                        })
                    },
                    autoFocus: true,
                    minLength: 3,
                    select: function (event, ui) {
                        if (ui.item.val == -1) {
                            $(this).val("");
                            return false;
                        }
                    },
                    autoFocus: true,
                    minLength: 3,
                    change: function (event, ui) {
                        if (ui.item == null) {
                            this.value = '';
                            alert('Please select City from the City list');
                        }
                    }
                });

      </script>


</asp:Content>
